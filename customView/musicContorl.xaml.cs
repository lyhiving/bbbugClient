﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace bbbug.com.customView
{
    /// <summary>
    /// musicContorl.xaml 的交互逻辑
    /// </summary>
    public partial class musicContorl : UserControl
    {
        bool voiceType = true;
        public musicContorl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //给老子转！
            RotateTransform rotate = new RotateTransform();
            ep.RenderTransform = rotate;
            ep.RenderTransformOrigin = new Point(0.5, 0.5);
            Storyboard story = new Storyboard();
            DoubleAnimation da = new DoubleAnimation(0, 360, new Duration(TimeSpan.FromSeconds(3)));
            Storyboard.SetTarget(da, ep);
            //欧拉角
            Storyboard.SetTargetProperty(da, new PropertyPath("RenderTransform.Angle"));
            da.RepeatBehavior = RepeatBehavior.Forever;
            story.Children.Add(da);
            story.Begin();
            ImageBrush brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(@"imgSource/16_机器人_Red.png", UriKind.Relative));
            voinceBtn.Background = brush;
        }
        public void changeImg(string imgSource)
        {
            ImageBrush brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(imgSource, UriKind.Relative));
            ep.Fill = brush;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //控制音量的
            if (voiceType)
            {
                //静音操作
                ImageBrush brush = new ImageBrush();
                brush.ImageSource = new BitmapImage(new Uri(@"imgSource/16_静音.png", UriKind.Relative));
                voinceBtn.Background = brush;
                voiceType = false;
            }
            else
            {
                //开启音量
                ImageBrush brush = new ImageBrush();
                brush.ImageSource = new BitmapImage(new Uri(@"imgSource/16_音量.png", UriKind.Relative));
                voinceBtn.Background = brush;
                voiceType = true;
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
