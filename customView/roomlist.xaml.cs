﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace customLibry
{
    /// <summary>
    /// UserControl1.xaml 的交互逻辑
    /// </summary>
    public partial class UserControl1 : UserControl
    {
        public UserControl1()
        {
            InitializeComponent();
        }
        public void setRoomName(string roomname)
        {
            roomName.Text = roomname;
        }
        public void setOwnerName(string ownername)
        {
            ownerName.Content = ownername;
        }
        public void setroomType(string source)
        {
            
            roomtype.Source = new BitmapImage(new Uri(source, UriKind.Relative));
        }
        public void setRoomId(string roomid)
        {
            roomId.Content = $"ID:{roomid}";
        }
        public void setPersonCount(string personcount)
        {
            personCount.Content = $"({personcount})";
        }
        public void setNotice(string notice)
        {
            notieLabel.Content = notice;
        }
        public void setOwnerImage(string user_Id)
        { 
            ownerImage.Source = new BitmapImage(new Uri($"{Environment.CurrentDirectory}\\cache\\{user_Id}.png"));
        }
    }
}
