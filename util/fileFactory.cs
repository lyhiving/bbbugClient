﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Text;

namespace bbbug.com.util
{
    class fileFactory
    {
        public void imgDown(string path, string target)
        {
            if (!Directory.Exists($"{Environment.CurrentDirectory}\\cache"))
            {
                Directory.CreateDirectory($"{Environment.CurrentDirectory}\\cache");
            }
            WebClient webClient = new WebClient();
            Byte[] imgData = webClient.DownloadData(target);
            Stream ms = new MemoryStream(imgData);
            ms.Position = 0;
            Image img = Image.FromStream(ms);
            string savepath = $"{Environment.CurrentDirectory}\\cache\\{path}";
            img.Save(savepath, ImageFormat.Jpeg);
        }
    }
}
