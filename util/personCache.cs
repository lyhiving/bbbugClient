﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace bbbug.com.util
{
    class personCache
    {

        netFactory nf = new netFactory();
        // 获取人员头像接口 并缓存在本地 根据人员id来
        public void getPersonImg(string userid)
        {
            if (!File.Exists($"{Environment.CurrentDirectory}\\cache\\{userid}.png"))
            {
                JObject jObject = new JObject { { "plat", "PC" }, { "version", "1.0" }, { "user_id", userid } };
                JObject reJ = (JObject)JsonConvert.DeserializeObject(nf.PostFunction("/user/getUserInfo", jObject.ToString()));
                if (reJ["code"].ToString().Equals("200"))
                {
                    //获取成功

                    JObject dataJobjct = (JObject)reJ["data"];
                    string usr_headURL = dataJobjct["user_head"].ToString();
                    if (usr_headURL.Equals(""))
                    {
                        usr_headURL = "https://bbbug.com/images/nohead.jpg";
                    }
                    new fileFactory().imgDown($"{userid}.png", usr_headURL);
                }
            }

        }




    }
}
