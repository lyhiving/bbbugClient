﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace bbbug.com.util
{
    class LayoutFactory
    {
        public StackPanel drawRoomList(JObject ja, int c)
        {
            personCache pC = new personCache();
            string user_Id = ja["user_id"].ToString();
            pC.getPersonImg(user_Id);
            //方案布局
            StackPanel sMain = new StackPanel();
            //sMain.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 240, 245));
            sMain.HorizontalAlignment = HorizontalAlignment.Left;
            sMain.VerticalAlignment = VerticalAlignment.Top;
            sMain.Orientation = Orientation.Vertical; // 垂直排序 分为两行 添加两行内容
            sMain.Width = 280;
            sMain.Height = 80;
            Thickness ts = new Thickness();
            ts.Left = 0;
            ts.Top = c * 83;
            ts.Right = 0;
            ts.Bottom = 0;
            sMain.Margin = ts;
            //分栏
            // 第一栏     [ 名称    id ]
            StackPanel sOne = new StackPanel();
            sOne.Orientation = Orientation.Horizontal;//平行
            sMain.Children.Add(sOne);
            // 第二列 [头像 房主名  在线人数 房间类型  其他小图标] GRID分两行
            StackPanel sTwo = new StackPanel();
            sTwo.Orientation = Orientation.Horizontal;
            sTwo.HorizontalAlignment = HorizontalAlignment.Left;
            sTwo.VerticalAlignment = VerticalAlignment.Top;
            sMain.Children.Add(sTwo);
            Grid spGridOne = new Grid(); // 再加三列
            //spGridOne.ShowGridLines = true;
            spGridOne.HorizontalAlignment = HorizontalAlignment.Left;
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(120, GridUnitType.Pixel);
            ColumnDefinition c2 = new ColumnDefinition();
            c2.Width = new GridLength(100, GridUnitType.Pixel);
            ColumnDefinition c3 = new ColumnDefinition();
            c3.Width = new GridLength(80, GridUnitType.Pixel);
            spGridOne.ColumnDefinitions.Add(c1);
            spGridOne.ColumnDefinitions.Add(c2);
            spGridOne.ColumnDefinitions.Add(c3);
            //添加内容 名称 占地 1 大小48px
            Label roomName = new Label();
            roomName.Content = $"{ja["room_name"]}";
            roomName.FontWeight = FontWeights.Bold;
            roomName.Width = 150;
            //sMain.Children.Add(roomName);
            spGridOne.Children.Add(roomName);
            Grid.SetColumn(roomName, 0);
            Grid.SetRow(roomName, 0);
            Grid.SetColumnSpan(roomName, 1);
            //Grid.SetRowSpan(roomName, 4);
            //添加内容 ID 占地大小 1 大小 48px
            Label roomId = new Label();
            roomId.FontSize = 10;
            roomId.Content = $"ID:{ja["room_id"].ToString()}";
            roomId.Background = new SolidColorBrush(Color.FromRgb(192, 192, 192));
            roomId.HorizontalContentAlignment = HorizontalAlignment.Left;
            spGridOne.Children.Add(roomId);
            Grid.SetColumn(roomId, 2);
            Grid.SetRow(roomId, 0);
            Grid.SetColumnSpan(roomId, 1);
            Grid.SetRowSpan(roomId, 1);
            sMain.Children.Add(spGridOne);
            //Grid 
            Grid spGridTwo = new Grid();
            //spGridTwo.ShowGridLines = true;
            ColumnDefinition d1 = new ColumnDefinition();
            d1.Width = new GridLength(48, GridUnitType.Pixel);
            spGridTwo.ColumnDefinitions.Add(d1);
            ColumnDefinition d2 = new ColumnDefinition();
            d2.Width = new GridLength(128, GridUnitType.Pixel);
            spGridTwo.ColumnDefinitions.Add(d2);

            //添加内容 头像 占地 2*2 48*48 
            Image im = new Image();
            im.Width = 48;
            im.Height = 48;
            im.Source = new BitmapImage(new Uri($"{Environment.CurrentDirectory}\\cache\\{user_Id}.png"));
            //im.HorizontalAlignment = HorizontalAlignment.Left;
            spGridTwo.Children.Add(im);
            Grid.SetColumn(im, 0);
            Grid.SetRow(im, 1);
            Grid.SetColumnSpan(im, 1);
            Grid.SetRowSpan(im, 1);
            /*sMain.Children.Add(spGridTwo);*/
            //后面跟一个StackGrid
            StackPanel s2p = new StackPanel();
            s2p.Orientation = Orientation.Vertical; // 垂直排列 分为两行
            s2p.Width = 230;
            spGridTwo.Children.Add(s2p);
            Grid.SetColumn(s2p, 1);
            Grid.SetRow(s2p, 1);
            Grid.SetColumnSpan(s2p, 2);
            Grid.SetRowSpan(s2p, 1);
            
            sMain.Children.Add(spGridTwo);
            return sMain;
        }
    }
}
