﻿using bbbug.com.util;
using customLibry;
using MahApps.Metro.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace bbbug.com
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {
        public string token = "";
        public MainWindow()
        {
            
            InitializeComponent();
        }

        public void loadingSource()
        {
            // 加载
           
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            login l = new login(this);
            this.Visibility = Visibility.Hidden;
            l.Show();
            //Hide();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.ToggleFlyout(0);
            getRoomList();
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.ToggleFlyout(1);
        }
        private void ToggleFlyout(int index)
        {
            var flyout = this.Flyouts1.Items[index] as Flyout;
            if (flyout == null)
            {
                return;
            }

            flyout.IsOpen = !flyout.IsOpen;
        }
        /// <summary>
        /// /room/hotRooms
        /// </summary>
        private void getRoomList()
        {
            netFactory nf = new netFactory();
            JObject jObject = new JObject { { "plat", "PC" }, { "version", "1.0" }, { "access_token", token} };
            JObject rObject = JObject.Parse(nf.PostFunction("/room/hotRooms", jObject.ToString()));
            if (rObject["code"].ToString().Equals("200"))
            {
                JArray roomArray = rObject["data"].Value<JArray>();
                // 绘制房间列表信息
                personCache pC = new personCache();
                foreach(JObject ja in roomArray)
                {
                    string user_Id = ja["user_id"].ToString();
                    pC.getPersonImg(user_Id);
                    UserControl1 roomlist = new UserControl1();
                    roomlist.Width = 300;
                    roomlist.setRoomName(HttpUtility.UrlDecode(ja["room_name"].ToString())); //房间名称
                    roomlist.setroomType(@"./imgSource/16_耳机_Red.png");
                    roomlist.setRoomId(ja["room_id"].ToString());
                    roomlist.setOwnerImage(user_Id);
                    roomlist.setOwnerName(HttpUtility.UrlDecode(ja["user_name"].ToString()));
                    roomlist.setPersonCount(ja["room_online"].ToString());
                    roomlist.setNotice(HttpUtility.UrlDecode(ja["room_notice"].ToString()));
                    mg.Children.Add(roomlist);
                }
            }


        }

        
    }
}
